package com.example.tugas4papb;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    RecyclerView rv;
    public static String TAG = "RV";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setTitle("Tugas 4 PAPB | 215150401111037");
        rv = findViewById(R.id.rv);
        ArrayList<Mahasiswa> data = getData();
        MahasiswaAdapter adapter = new MahasiswaAdapter(this, data);
        rv.setAdapter(adapter);
        rv.setLayoutManager(new LinearLayoutManager(this));
        Button btSimpan = findViewById(R.id.bt1);
        btSimpan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText txtNIM = findViewById(R.id.addNim);
                EditText txtNama = findViewById(R.id.addNama);
                String NIM = txtNIM.getText().toString();
                String Nama = txtNama.getText().toString();

                if (!NIM.isEmpty() && !Nama.isEmpty()) {
                    Mahasiswa mhs = new Mahasiswa();
                    mhs.nim = NIM;
                    mhs.nama = Nama;
                    data.add(mhs);
                    adapter.notifyDataSetChanged();
                    txtNIM.setText("");
                    txtNama.setText("");
                }
            }
        });
    }

    public ArrayList getData() {
        ArrayList<Mahasiswa> data = new ArrayList<>();
        List<String> nim = Arrays.asList(getResources().getStringArray(R.array.NIM));
        List<String> nama = Arrays.asList(getResources().getStringArray(R.array.Nama));
        for (int i = 0; i < nim.size(); i++) {
            Mahasiswa mhs = new Mahasiswa();
            mhs.nim = nim.get(i);
            mhs.nama = nama.get(i);
            Log.d(TAG,"getData "+mhs.nim);
            data.add(mhs);
        }
        return data;
    }
}