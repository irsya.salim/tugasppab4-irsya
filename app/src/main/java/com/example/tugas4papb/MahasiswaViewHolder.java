package com.example.tugas4papb;

import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class MahasiswaViewHolder extends RecyclerView.ViewHolder {
    TextView tvNim;
    TextView tvNama;

    public MahasiswaViewHolder(@NonNull View itemView) {
        super(itemView);
        tvNim = itemView.findViewById(R.id.tvNIM);
        tvNama = itemView.findViewById(R.id.tvNama);
    }
}
